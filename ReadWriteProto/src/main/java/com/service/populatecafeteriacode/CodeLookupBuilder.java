package com.service.populatecafeteriacode;

import com.util.protoobjects.BuildingOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile.Reader;

import java.io.IOException;
import java.util.HashMap;

public class CodeLookupBuilder {

    public HashMap<String, String> get(String Path) {
        HashMap<String, String> buildingMap = new HashMap<>();
        Configuration conf = new Configuration();
        Reader reader = null;
        try {
            Path inFile = new Path(Path);
            reader = new Reader(conf, Reader.file(inFile), Reader.bufferSize(4096));
            buildLookup(buildingMap, reader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                IOUtils.closeStream(reader); //closing reader object
            }
        }
        return buildingMap;
    }

    private void buildLookup(HashMap<String, String> buildingMap, Reader reader) throws IOException {
        IntWritable key = new IntWritable(); //set key as IntWritable
        ImmutableBytesWritable value = new ImmutableBytesWritable();//set value ImmutableBytesWritable

        while (reader.next(key, value)) {
            BuildingOuterClass.Building.Builder e = BuildingOuterClass.Building.newBuilder().
                    mergeFrom(value.get());
            int building_code = Integer.parseInt(String.valueOf(e.getBuildingCode()));
            String cafeteria_code = e.getCafeteriaCode();
            //set buildingMap with building_code and building_code
            buildingMap.put(String.valueOf(building_code), cafeteria_code);
        }
    }
}
