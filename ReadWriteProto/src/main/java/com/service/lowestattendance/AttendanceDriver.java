package com.service.lowestattendance;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AttendanceDriver extends Configured implements Tool {
    private static final String ATTENDANCE_JOB_NAME = "Lowest Attendance";
    private static final String ATTENDANCE_TABLE_NAME = "employee";
    private static final String ATTENDANCE_PATH ="hdfs://localhost:8020/Attendance";


    public int run(String[] arg0) {
        List<Scan> scans = new ArrayList<>();
        Scan scan1 = new Scan();
        //set table name attribute in scan
        scan1.setAttribute("scan.attributes.table.name", Bytes.toBytes(ATTENDANCE_TABLE_NAME));
        scans.add(scan1);
        HBaseConfiguration conf = new HBaseConfiguration();
        try {
            Job job = new Job(conf, ATTENDANCE_JOB_NAME);
            job.setJarByClass(AttendanceDriver.class);
            FileOutputFormat.setOutputPath(job, new Path(ATTENDANCE_PATH)); //set output path
            job.setOutputKeyClass(IntWritable.class); //set output key class
            job.setOutputValueClass(IntWritable.class); //set output value class
            job.setJarByClass(AttendanceDriver.class);
            //read hbase table and send it to mapper
            TableMapReduceUtil.initTableMapperJob(scans, AttendanceMapper.class, IntWritable.class,
                    ImmutableBytesWritable.class, job);
            job.setReducerClass(AttendanceReducer.class); //set reducer class

            job.waitForCompletion(true);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
