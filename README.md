**——————How to get started ——————**

**Module 1 "HdfsAndHbase" contains**

1. Hbase and HDFS assignment 
Using hdfs and Hbase java api :
Create 100 csv files having People information in it. Store them to HDFS and write a java
Program to load these csv files from HDFS, read column values and load into HBASE
Sample People info :
Name, age, company, building_code, phone_number, address

2. MR and Hbase assignment :
Create a MR job to read files from HDFS and count the frequency of each word in the file.
Create a MR job to load data from the above created HBASE people table and write it back to a
new HBASE table using bulk upload.


**To run Module1:**
Go to -  HdfsAndHbase/src/main/java/com/

Run - Main.java



**Module 2 "ReadWriteProto" contains**

3. Proto assignment :
Create a java program to read data from csv and then set it to proto objects and print them
using these proto objects.
Proto files to be included :
Employee.proto
Example fields can be : name, employee_id, building_code, floor_number (this should be
enum), salary, department
etc
Building.proto
Example fields can be : building_code, total_floors, companies_in_the_building, cafteria_code
Create data for 100 people and 10 building with similar signature.


4. HBase, proto and MR combined assignment:
4.1 (Optional) - Create a Mapreduce job to use the above people and building data, load proto
objects with those values and then write it back to HBase.
While doing this drill, update the employee.proto file to include cafeteria_code as a new field.
Leave the value blank for now.
Build jars for the above proto project, this will be used for downstream assignments
4.2 - Create a MR job using the above created table (employee and building).
- Read both the tables using there jars added to the lib
- Parse its value using proto objects
- Join both the tables on the basis of building_code and enrich the cafteria_code for each
employee using the cafteria_code available in the building proto object and write it back
to hbase table.
- Construct a decent unique rowkey for denoting each row of value.
4.3 (Optional) - Create a proto attendance.proto with fields like employee_id, date and generate
random entries for different dates for each employee.
For every building find the employee with the lowest attendance.


**To run Module2:**
Go to -  ReadWriteProto/src/main/java/com/

Run - Main.java